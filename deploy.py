from typing import NamedTuple
from kfp.v2.dsl import component, pipeline

VERTEX_PIPELINES_ROOT = 'gs://coe-ie-dsg-training-lab-vertex-pipelines-root-bucket/'
PIPELINE_NAME = 'demo-deployment'


@component(packages_to_install=["google-cloud-aiplatform"])
def create_endpoint(project_id: str, region: str, display_name: str) \
        -> NamedTuple("Outputs", [("endpoint_resource_name", str)]):
    from google.cloud import aiplatform as vertex_ai

    request = vertex_ai.Endpoint.create(
        display_name=display_name,
        project=project_id,
        location=region
    )

    return (request.resource_name,)


@component(packages_to_install=["google-cloud-aiplatform"])
def deploy_model_to_endpoint(project_id: str, region: str, endpoint_instance: str, model_id: str,
                             model_display_name: str, traffic_percentage: int):
    from google.cloud import aiplatform as vertex_ai

    model_resource_name = f"projects/{project_id}/locations/{region}/models/{model_id}"

    model = vertex_ai.Model(model_resource_name)
    endpoint = vertex_ai.Endpoint(endpoint_instance)

    endpoint.deploy(
        model=model,
        deployed_model_display_name=model_display_name,
        traffic_percentage=traffic_percentage,
        traffic_split=None,
        machine_type='n1-standard-4',
        min_replica_count=1,
        max_replica_count=1,
        accelerator_type=None,
        accelerator_count=None,
    )


@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline_fn(project_id: str, region: str,
                model_a_id: str, model_a_name: str, model_a_percentage: int):

    create_endpoint_op = create_endpoint(
        project_id=project_id,
        region=region,
        display_name="demo_endpoint",
    )

    deploy_first_op = deploy_model_to_endpoint(
        project_id=project_id,
        region=region,
        endpoint_instance=create_endpoint_op.outputs['endpoint_resource_name'],
        model_id=model_a_id,
        model_display_name=model_a_name,
        traffic_percentage=model_a_percentage
    )


if __name__ == "__main__":
    from kfp.v2 import compiler

    compiler.Compiler().compile(
        pipeline_func=pipeline_fn,
        package_path=f'{PIPELINE_NAME}.json')

    from google.cloud import aiplatform as vertex_ai
    from google.oauth2 import service_account
    from datetime import datetime

    TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
    PROJECT_ID = "coe-ie-dsg-training-lab"
    LOCATION = "europe-west2"
    SERVICE_ACCOUNT = "vertex-ai-sdk@coe-ie-dsg-training-lab.iam.gserviceaccount.com"


    job = vertex_ai.PipelineJob(
        project=PROJECT_ID,
        location=LOCATION,
        display_name=f"{PIPELINE_NAME}-{TIMESTAMP}",
        template_path=f'{PIPELINE_NAME}.json',
        enable_caching=True,
        parameter_values={
            "project_id": PROJECT_ID,
            "region": LOCATION,
            "model_a_name": "demo-tabular-automl-model",  
            "model_a_id": "1032837242667663360", 
            "model_a_percentage": 100
        }
    )

    job.submit(service_account=SERVICE_ACCOUNT)
