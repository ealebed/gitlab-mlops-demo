import os
import glob
import pickle
import logging
from typing import List
from pathlib import Path
from tempfile import mkdtemp

import pandas as pd
import xgboost as xgb
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, OrdinalEncoder

# without this no logs will be visible in GCP
logging.getLogger().setLevel(logging.INFO)


def gcs_fuse_path(gcs_path: str) -> str:
    if gcs_path.startswith("gs://"):
        return gcs_path.replace("gs://", '/gcs/')
    else:
        return gcs_path


def wildcard_to_list(wildcard_path: str) -> List[str]:
    return [path for path in glob.glob(wildcard_path)]


variables = [
    'AIP_TRAINING_DATA_URI',  # The location that your training data is stored at.
    'AIP_VALIDATION_DATA_URI',  # The location that your validation data is stored at.
    'AIP_TEST_DATA_URI',  # The location that your test data is stored at.
    'AIP_MODEL_DIR',  # a Cloud Storage URI of a directory intended for saving model artifacts
]

for variable in variables:
    os.environ[variable] = gcs_fuse_path(os.environ[variable])
    logging.info(f"{variable} --> {os.environ[variable]}")

DUMMY_COLUMN = "DataSplit"
TARGET_COLUMN = "Deposit"
NUMERIC_COLUMNS = ['Age', 'Balance', 'Duration', 'PDays']
CATEGORICAL_COLUMNS = ['Job', 'MaritalStatus', 'Education', 'Default', 'Housing', 'Loan',
                       'Contact', 'Day', 'Month', 'Campaign', 'Previous', 'POutcome']

FEATURE_COLUMNS = NUMERIC_COLUMNS + CATEGORICAL_COLUMNS

dataset = {}
for key, src in zip(['train', 'valid', 'test'], ['AIP_TRAINING_DATA_URI', 'AIP_VALIDATION_DATA_URI', 'AIP_TEST_DATA_URI']):
    paths = wildcard_to_list(os.environ[src])
    dataset[key] = pd.concat([pd.read_csv(path) for path in paths], ignore_index=True)
    dataset[key].drop(columns=DUMMY_COLUMN, inplace=True)

numeric_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy="median")),
    ('scaler', StandardScaler())
])

categorical_transformer = OrdinalEncoder()

preprocessing = ColumnTransformer(transformers=[
    ("numeric", numeric_transformer, NUMERIC_COLUMNS),
    ("categorical", categorical_transformer, CATEGORICAL_COLUMNS)],
    remainder='passthrough'
)

X, y = dict(), dict()
for key in ['train', 'valid', 'test']:
    X[key], y[key] = dataset[key].drop(columns=[TARGET_COLUMN]), dataset[key][TARGET_COLUMN]

preprocessing.fit(pd.concat([X[key] for key in ['train', 'valid', 'test']]))

model = xgb.XGBClassifier(
    objective="binary:logistic",
    n_estimators=100, learning_rate=0.2, max_depth=5,
    n_jobs=-1, random_state=0, subsample=0.8, tree_method='hist'
)

pipeline = Pipeline(steps=[
        ('preprocessing', preprocessing),
        ('model', model)])

pipeline.fit(
    X=X['train'], y=y['train'],
    model__eval_set=[(preprocessing.transform(X['valid']), y['valid'])],
    model__eval_metric='logloss',
    model__early_stopping_rounds=15,
)

pipeline.predict(X['train'])
pipeline.predict(X['valid'])

# create the folder we need if it doesn't exist
Path(os.environ['AIP_MODEL_DIR']).mkdir(parents=True, exist_ok=True)

# save the model
with open(os.path.join(os.environ['AIP_MODEL_DIR'], 'model.pkl'), 'wb') as output_file:
    pickle.dump(pipeline, output_file)
