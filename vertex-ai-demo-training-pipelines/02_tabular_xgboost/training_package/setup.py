from setuptools import setup, find_packages

setup(
    name='xgboost-custom-training-demo',
    version='7.0',
    description='Training application for the Banking Managed Dataset',
    packages=find_packages(),
)
