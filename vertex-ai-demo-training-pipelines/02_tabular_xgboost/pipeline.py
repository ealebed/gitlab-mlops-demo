from typing import NamedTuple
from kfp.v2.dsl import component, pipeline
from kfp.v2.dsl import Input, Output, Model, ClassificationMetrics, Metrics, HTML, Artifact, Dataset

VERTEX_PIPELINES_ROOT = 'gs://coe-ie-dsg-training-lab-vertex-pipelines-root-bucket/'
PIPELINE_NAME = 'demo-tabular-dataset-xgboost'


@component(packages_to_install=["google-cloud-aiplatform"])
def get_dataset_source_artifact(project_id: str, region: str, dataset_id: str, dataset_artifact: Output[Dataset]):
    from shutil import copyfile
    from distutils.dir_util import copy_tree
    from google.cloud import aiplatform as vertex_ai

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    dataset_resource_name = resource_name(project_id, region, dataset_id, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_resource_name)

    dataset_uri = dataset.to_dict()['metadata']['inputConfig']['gcsSource']['uri'][0]
    dataset_path = dataset_uri.replace('gs://', '/gcs/')

    try:
        copyfile(dataset_path, dataset_artifact.path)
    except IsADirectoryError:
        copy_tree(dataset_path, dataset_artifact.path)


@component(packages_to_install=["tensorflow-data-validation", "pandas"])
def data_validation(dataset_artifact: Input[Dataset], statistics_html: Output[HTML], features_html: Output[HTML],
                    cardinality_html: Output[HTML], schema_artifact: Output[Artifact], frozen_schema_path: str,
                    anomalies_html: Output[HTML]):

    import tensorflow_data_validation as tfdv
    from tensorflow_data_validation.utils.display_util import get_statistics_html
    from tensorflow_data_validation.utils.display_util import get_schema_dataframe
    from tensorflow_data_validation.utils.display_util import get_anomalies_dataframe

    statistics = tfdv.generate_statistics_from_csv(dataset_artifact.path)
    html = get_statistics_html(statistics)
    with open(statistics_html.path, 'w') as f:
        f.write(html)

    # if you have already manually generated the schema and validated it
    if frozen_schema_path != "None":
        schema = tfdv.load_schema_text(frozen_schema_path)
        anomalies = tfdv.validate_statistics(statistics, schema)
        df = get_anomalies_dataframe(anomalies)
        html = df.to_html()
        with open(anomalies_html.path, 'w') as f:
            f.write(html)

    # otherwise we rely on automatic schema generation
    else:
        schema = tfdv.infer_schema(statistics)
        schema.default_environment.append('TRAINING')
        schema.default_environment.append('SERVING')

        tfdv.get_feature(schema, 'DataSplit').not_in_environment.append('SERVING')
        tfdv.get_feature(schema, 'DataSplit').not_in_environment.append('TRAINING')
        tfdv.get_feature(schema, 'Deposit').not_in_environment.append('SERVING')

        tfdv.write_schema_text(schema, schema_artifact.path)

        features, cardinality = get_schema_dataframe(schema)
        for df, output in zip([features, cardinality], [features_html, cardinality_html]):
            html = df.to_html()
            with open(output.path, 'w') as f:
                f.write(html)


@component(packages_to_install=["google-cloud-aiplatform"])
def train_model(project_id: str, region: str, display_name: str, dataset_id: str,
                python_package_gcs_uri: str, python_module_name: str, training_image: str, serving_image: str,
                staging_bucket: str) \
        -> NamedTuple("Outputs", [("model_instance", str)]):
    import logging
    from google.cloud import aiplatform as vertex_ai

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    logging.getLogger().setLevel(logging.INFO)

    dataset_instance = resource_name(project_id, region, dataset_id, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_name=dataset_instance)

    training_job = vertex_ai.CustomPythonPackageTrainingJob(
        project=project_id,
        location=region,
        display_name=display_name,
        python_package_gcs_uri=python_package_gcs_uri,
        python_module_name=python_module_name,
        container_uri=training_image,
        model_serving_container_image_uri=serving_image,
        staging_bucket=staging_bucket
    )

    model = training_job.run(
        dataset=dataset,
        predefined_split_column_name="DataSplit",
        replica_count=1,
        machine_type="n1-standard-4",
        accelerator_type="ACCELERATOR_TYPE_UNSPECIFIED",
        accelerator_count=0,
    )

    return (model.resource_name,)


@component(packages_to_install=["google-cloud-aiplatform"])
def export_model_artifact(model_resource_name: str, model_artifact: Output[Model]):
    from shutil import copy2
    from distutils.dir_util import copy_tree
    from google.cloud import aiplatform as vertex_ai

    model = vertex_ai.Model(model_resource_name)

    full_uri = model.export_model(
        export_format_id='custom-trained',
        artifact_destination=model_artifact.uri
    )
    model_path = full_uri['artifactOutputUri'].replace('gs://', '/gcs/')

    try:
        copy2(model_path, model_artifact.path)
    except IsADirectoryError:
        copy_tree(model_path, model_artifact.path)


@component(
    base_image='python:3.8.10',
    packages_to_install=['scikit-learn==0.23.1', 'xgboost==1.1.1', 'pandas==1.0.4'])
def evaluate_classification_model(dataset_artifact: Input[Dataset], model_artifact: Input[Model],
                                  c_metrics: Output[ClassificationMetrics], metrics: Output[Metrics]):
    import glob
    import pickle
    import logging
    import pandas as pd
    from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve, average_precision_score

    logging.getLogger().setLevel(logging.INFO)

    model_path = glob.glob(f'{model_artifact.path}/*.*')
    logging.info(f"model_path = {model_path}")

    with open(model_path[0], "rb") as file:
        model = pickle.load(file)

    logging.info(f"model -> {model}")

    df = pd.read_csv(dataset_artifact.path)
    df = df[df["DataSplit"] == "TEST"]

    y_true = df["Deposit"].tolist()
    X = df.drop(columns=["Deposit", 'DataSplit'])

    logging.info(f"X.shape = {X.shape}")
    logging.info(f"X.columns = {X.columns}")
    logging.info(f"y_true.shape = ({len(y_true)},)")

    y_pred = model.predict(X)
    y_proba = model.predict_proba(X)

    logging.info(y_pred.shape, y_proba.shape)

    cm = confusion_matrix(y_true, y_pred)
    auc = roc_auc_score(y_true, y_proba[:, 1].tolist())
    aucpr = average_precision_score(y_true, y_proba[:, 1].tolist())

    fpr, tpr, thresholds = roc_curve(y_true, y_proba[:, 1].tolist())

    logging.info(cm, auc, aucpr)

    metrics.log_metric("auRoc", auc)
    metrics.log_metric("auPrc", aucpr)

    c_metrics.log_roc_curve(fpr.tolist(), tpr.tolist(), thresholds.tolist())
    c_metrics.log_confusion_matrix(
        categories=["No Deposit", "Deposit"],
        matrix=cm.tolist())


@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline_fn(project_id: str, region: str, dataset_id: str, python_package_path: str, python_module_name: str,
                training_image: str, serving_image: str):

    dataset_import_op = get_dataset_source_artifact(
        project_id=project_id, region=region, dataset_id=dataset_id
    )

    data_validation_op = data_validation(
        dataset_artifact=dataset_import_op.outputs['dataset_artifact'], frozen_schema_path="None")

    data_validation_op.after(dataset_import_op)

    train_op = train_model(
        project_id=project_id,
        region=region,
        display_name="demo_xgboost_model-v6",
        dataset_id=dataset_id,
        python_package_gcs_uri=python_package_path,
        python_module_name=python_module_name,
        training_image=training_image,
        serving_image=serving_image,
        staging_bucket=f"{VERTEX_PIPELINES_ROOT}{PIPELINE_NAME}/staging_bucket"
    )

    export_model_op = export_model_artifact(
        model_resource_name=train_op.outputs["model_instance"],
    )

    evaluate_op = evaluate_classification_model(
        dataset_artifact=dataset_import_op.outputs["dataset_artifact"],
        model_artifact=export_model_op.outputs["model_artifact"]
    )


if __name__ == "__main__":
    from kfp.v2 import compiler

    compiler.Compiler().compile(
        pipeline_func=pipeline_fn,
        package_path=f'{PIPELINE_NAME}.json')

    from google.cloud import aiplatform as vertex_ai
    from google.oauth2 import service_account
    from datetime import datetime

    TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
    PROJECT_ID = "coe-ie-dsg-training-lab"
    LOCATION = "europe-west2"

    job = vertex_ai.PipelineJob(
        project=PROJECT_ID,
        location=LOCATION,
        display_name=f"{PIPELINE_NAME}-{TIMESTAMP}",
        template_path=f'{PIPELINE_NAME}.json',
        enable_caching=True,
        parameter_values={
            "project_id": PROJECT_ID,
            "region": LOCATION,
            "dataset_id": "2010435021155860480",
            "python_package_path": "gs://coe-ie-dsg-training-lab-demo/source_distributions/xgboost-custom-training-demo-7.0.tar.gz",
            "python_module_name": "trainer.task",
            "training_image": "us-docker.pkg.dev/vertex-ai/training/xgboost-cpu.1-1:latest",
            "serving_image": "us-docker.pkg.dev/vertex-ai/prediction/xgboost-cpu.1-1:latest",
        }
    )

    job.submit()
