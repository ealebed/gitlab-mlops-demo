from setuptools import setup, find_packages

setup(
    name='tensorflow-custom-training-demo-hyperparams',
    version='2.0',
    description='Training application for the Banking Managed Dataset',
    packages=find_packages(),
    include_package_data=True
)
