import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers


def create_tuning_dataset(csv_path, num_rows, batch_size, target_column):
    df = pd.read_csv(csv_path)
    df = df.head(num_rows)
    df.to_csv('temp.csv', index=False)

    AUTO = tf.data.experimental.AUTOTUNE
    dataset = tf.data.experimental.make_csv_dataset(
        file_pattern='temp.csv', header=True, label_name=target_column,
        batch_size=batch_size, num_parallel_reads=AUTO, prefetch_buffer_size=256, num_epochs=1)

    return dataset


def get_normalization_layer(name, dataset):
    normalizer = layers.experimental.preprocessing.Normalization(axis=None)
    feature_dataset = dataset.map(lambda x, y: x[name])
    normalizer.adapt(feature_dataset)
    return normalizer


def get_one_hot_encoding_layer(name, dataset, dtype, max_tokens=None):
    if dtype == 'string':
        index = layers.experimental.preprocessing.StringLookup(max_tokens=max_tokens)
    else:
        index = layers.experimental.preprocessing.IntegerLookup(max_tokens=max_tokens)

    feature_dataset = dataset.map(lambda x, y: x[name])
    index.adapt(feature_dataset)
    encoder = layers.experimental.preprocessing.CategoryEncoding(num_tokens=index.vocabulary_size(),
                                                                 output_mode="one_hot")
    return lambda feature: encoder(index(feature))


def create_dense_inputs_and_features(dataset, numeric_columns):
    inputs = []
    dense_features = []

    for column in numeric_columns:
        input_layer = layers.Input(shape=(1,), name=column, dtype='float32')
        normalization_layer = get_normalization_layer(column, dataset)
        numeric_feature = normalization_layer(input_layer)

        inputs.append(input_layer)
        dense_features.append(numeric_feature)

    return inputs, dense_features


def create_sparse_inputs_and_features(dataset, categorical_columns, ordinal_columns):
    inputs = []
    sparse_features = []

    for column in categorical_columns:
        dtype = 'string'
        input_layer = layers.Input(shape=(1,), name=column, dtype=dtype)
        encoding_layer = get_one_hot_encoding_layer(column, dataset, dtype, max_tokens=30)
        numeric_feature = encoding_layer(input_layer)

        inputs.append(input_layer)
        sparse_features.append(numeric_feature)

    for column in ordinal_columns:
        dtype = 'int32'
        input_layer = layers.Input(shape=(1,), name=column, dtype=dtype)
        encoding_layer = get_one_hot_encoding_layer(column, dataset, dtype, max_tokens=30)
        numeric_feature = encoding_layer(input_layer)

        inputs.append(input_layer)
        sparse_features.append(numeric_feature)

    return inputs, sparse_features


def wide_and_deep(inputs, sparse_features, dense_features, learning_rate, num_layers, num_neurons):
    wide = layers.concatenate(sparse_features)
    deep = layers.concatenate(dense_features)

    for layer in range(num_layers):
        deep = layers.Dense(num_neurons, activation='relu')(deep)

    combined = layers.concatenate([wide, deep])
    output = layers.Dense(1, activation='sigmoid')(combined)

    model = tf.keras.Model(inputs, output)
    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
        loss='binary_crossentropy',
        metrics='accuracy')

    model.summary()
    return model


def create_model(dataset, numerical_columns, categorical_columns, ordinal_columns,
                 learning_rate, num_layers, num_neurons):
    dense_inputs, dense_features = create_dense_inputs_and_features(dataset, numerical_columns)
    sparse_inputs, sparse_features = create_sparse_inputs_and_features(dataset, categorical_columns, ordinal_columns)

    return wide_and_deep(
        inputs=dense_inputs + sparse_inputs,
        sparse_features=sparse_features,
        dense_features=dense_features,
        learning_rate=learning_rate,
        num_layers=num_layers,
        num_neurons=num_neurons
    )
