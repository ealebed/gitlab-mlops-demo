import os
import logging
import argparse

from trainer import utils
import tensorflow as tf

# without this no logs will be visible in Vertex AI
logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser(description='Parameters')
parser.add_argument('--epochs', type=int)
parser.add_argument('--learning_rate', type=float)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--num_layers', type=int)
parser.add_argument('--num_neurons', type=int)
parser.add_argument('--positive_class_weight', type=float)

args = parser.parse_args()

variables = [
    'AIP_DATA_FORMAT',  # The format that your dataset is exported in. Possible values include: jsonl, csv, or bigquery.
    'AIP_TRAINING_DATA_URI',  # The location that your training data is stored at.
    'AIP_VALIDATION_DATA_URI',  # The location that your validation data is stored at.
    'AIP_TEST_DATA_URI',  # The location that your test data is stored at.
    'AIP_MODEL_DIR',  # a Cloud Storage URI of a directory intended for saving model artifacts
    'AIP_CHECKPOINT_DIR',  # a Cloud Storage URI of a directory intended for saving checkpoints
    'AIP_TENSORBOARD_LOG_DIR',  # a Cloud Storage URI of a directory intended for saving TensorBoard logs
]

for variable in variables:
    logging.info(f'os.environ[{variable}] = {os.environ[variable]}')
    if variable not in os.environ:
        raise KeyError(f'The {variable} environment variable has not been set')

logging.info("loading the datasets")

DUMMY_COLUMN = "DataSplit"
TARGET_COLUMN = "Deposit"
NUMERIC_COLUMNS = ['Age', 'Balance', 'Duration', 'PDays']
CATEGORICAL_COLUMNS = ['Job', 'MaritalStatus', 'Education', 'Default', 'Housing', 'Loan', 'Contact', 'Month', 'POutcome']
ORDINAL_COLUMNS = ['Day', 'Campaign', 'Previous']

AUTO = tf.data.experimental.AUTOTUNE
dataset = {}

for ds, source in zip(['train', 'valid', 'test'], ['AIP_TRAINING_DATA_URI', 'AIP_VALIDATION_DATA_URI', 'AIP_TEST_DATA_URI']):
    dataset[ds] = tf.data.experimental.make_csv_dataset(
        file_pattern=os.environ[source], header=True, label_name=TARGET_COLUMN,
        batch_size=args.batch_size, num_parallel_reads=AUTO, prefetch_buffer_size=256, num_epochs=1)

    dataset[ds] = dataset[ds]
    dataset[ds].cache()

model = utils.create_model(
    dataset=dataset['train'],
    numerical_columns=NUMERIC_COLUMNS, categorical_columns=CATEGORICAL_COLUMNS, ordinal_columns=ORDINAL_COLUMNS,
    learning_rate=args.learning_rate, num_layers=args.num_layers, num_neurons=args.num_neurons)

logging.info('Defining Callbacks for Training')

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=os.environ['AIP_CHECKPOINT_DIR'],
    save_weights_only=False,
    monitor='accuracy',
    mode='max',
    save_best_only=True)
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=os.environ['AIP_TENSORBOARD_LOG_DIR'])
callbacks = [model_checkpoint_callback, tensorboard_callback]


logging.info('Start model training')
model.fit(dataset['train'], epochs=args.epochs, validation_data=dataset['valid'],
          callbacks=callbacks,
          class_weight={
              0: 1-args.positive_class_weight,
              1: args.positive_class_weight
          })

logging.info('Loading the best performing model')
best_model = tf.keras.models.load_model(os.environ['AIP_CHECKPOINT_DIR'])

logging.info('Evaluating the best model on the test dataset')
loss, accuracy = best_model.evaluate(dataset['test'])
logging.info(f'Test loss = {loss} / test accuracy = {accuracy}')

logging.info('Saving the model to GCS')
best_model.save(os.environ['AIP_MODEL_DIR'])

logging.info("ALL DONE!")
