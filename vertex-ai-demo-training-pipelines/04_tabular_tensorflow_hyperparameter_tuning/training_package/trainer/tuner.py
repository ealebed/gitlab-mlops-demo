import logging
import argparse
import hypertune
from trainer import utils

parser = argparse.ArgumentParser(description='Hyperparameter optimization')
parser.add_argument('--learning_rate', type=float)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--num_layers', type=int)
parser.add_argument('--num_neurons', type=int)
parser.add_argument('--dataset_path', type=str)
parser.add_argument('--trial_epochs', type=int)
parser.add_argument('--trial_num_records', type=int)
parser.add_argument('--positive_class_weight', type=float)

args = parser.parse_args()

# without this no logs will be visible in Vertex AI
logging.getLogger().setLevel(logging.INFO)

DUMMY_COLUMN = "DataSplit"
TARGET_COLUMN = "Deposit"
NUMERIC_COLUMNS = ['Age', 'Balance', 'Duration', 'PDays']
CATEGORICAL_COLUMNS = ['Job', 'MaritalStatus', 'Education', 'Default', 'Housing', 'Loan', 'Contact', 'Month', 'POutcome']
ORDINAL_COLUMNS = ['Day', 'Campaign', 'Previous']

dataset = utils.create_tuning_dataset(
    csv_path=args.dataset_path,
    target_column=TARGET_COLUMN,
    num_rows=args.trial_num_records,
    batch_size=args.batch_size).cache()

model = utils.create_model(
    dataset=dataset,
    numerical_columns=NUMERIC_COLUMNS, categorical_columns=CATEGORICAL_COLUMNS, ordinal_columns=ORDINAL_COLUMNS,
    learning_rate=args.learning_rate, num_layers=args.num_layers, num_neurons=args.num_neurons
)

model.fit(dataset, epochs=args.trial_epochs, class_weight={
    0: 1-args.positive_class_weight,
    1: args.positive_class_weight
})

loss, accuracy = model.evaluate(dataset)
logging.info(f'Test loss = {loss} / test accuracy = {accuracy}')

logging.info('Sending the metric to Vertex Ai')
hpt = hypertune.HyperTune()
hpt.report_hyperparameter_tuning_metric(
    hyperparameter_metric_tag='accuracy',
    metric_value=accuracy,
    global_step=10)
