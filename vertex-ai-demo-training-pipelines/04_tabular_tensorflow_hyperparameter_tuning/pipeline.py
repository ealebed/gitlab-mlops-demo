from typing import NamedTuple
from kfp.v2.dsl import component, pipeline, Output, Metrics, ClassificationMetrics, Model, HTML, Artifact

VERTEX_PIPELINES_ROOT = 'gs://coe-ie-dsg-training-lab-vertex-pipelines-root-bucket/'
PIPELINE_NAME = 'demo-tabular-dataset-advanced-tensorflow'


@component(packages_to_install=["google-cloud-aiplatform"])
def get_dataset_source_artifact(project_id: str, region: str, dataset_id: str) \
        -> NamedTuple("Outputs", [("dataset_artifact_path", str)]):
    from google.cloud import aiplatform as vertex_ai

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    dataset_resource_name = resource_name(project_id, region, dataset_id, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_resource_name)

    dataset_uri = dataset.to_dict()['metadata']['inputConfig']['gcsSource']['uri'][0]
    dataset_path = dataset_uri.replace('gs://', '/gcs/')

    return (dataset_path,)


@component(packages_to_install=["tensorflow-data-validation", "pandas"])
def data_validation(dataset_artifact_path: str, statistics_html: Output[HTML], features_html: Output[HTML],
                    cardinality_html: Output[HTML], schema_artifact: Output[Artifact], frozen_schema_path: str,
                    anomalies_html: Output[HTML]):

    import tensorflow_data_validation as tfdv
    from tensorflow_data_validation.utils.display_util import get_statistics_html
    from tensorflow_data_validation.utils.display_util import get_schema_dataframe
    from tensorflow_data_validation.utils.display_util import get_anomalies_dataframe

    statistics = tfdv.generate_statistics_from_csv(dataset_artifact_path)
    html = get_statistics_html(statistics)
    with open(statistics_html.path, 'w') as f:
        f.write(html)

    # if you have already manually generated the schema and validated it
    if frozen_schema_path != "None":
        schema = tfdv.load_schema_text(frozen_schema_path)
        anomalies = tfdv.validate_statistics(statistics, schema)
        df = get_anomalies_dataframe(anomalies)
        html = df.to_html()
        with open(anomalies_html.path, 'w') as f:
            f.write(html)

    # otherwise we rely on automatic schema generation
    else:
        schema = tfdv.infer_schema(statistics)
        schema.default_environment.append('TRAINING')
        schema.default_environment.append('SERVING')

        tfdv.get_feature(schema, 'DataSplit').not_in_environment.append('SERVING')
        tfdv.get_feature(schema, 'DataSplit').not_in_environment.append('TRAINING')
        tfdv.get_feature(schema, 'Deposit').not_in_environment.append('SERVING')

        tfdv.write_schema_text(schema, schema_artifact.path)

        features, cardinality = get_schema_dataframe(schema)
        for df, output in zip([features, cardinality], [features_html, cardinality_html]):
            html = df.to_html()
            with open(output.path, 'w') as f:
                f.write(html)


@component(packages_to_install=["google-cloud-aiplatform"])
def create_hyperparameter_optimization_job(project_id: str, region: str, dataset_artifact_path: str,
                                           training_image: str, training_package: str, tuner_module: str,
                                           max_trial_count: int, num_parallel_trials: int) \
        -> NamedTuple("Outputs", [("job_resource_name", str)]):
    from google.cloud import aiplatform as vertex_ai
    from google.cloud.aiplatform.gapic import StudySpec

    api_endpoint = f"{region}-aiplatform.googleapis.com"

    parent = f"projects/{project_id}/locations/{region}"
    client = vertex_ai.gapic.JobServiceClient(client_options={"api_endpoint": api_endpoint})

    hyperparameter_tuning_job_spec = {
        "display_name": "demo-tensorflow-hyperparameter-tuning-job",
        "max_trial_count": 10,
        "parallel_trial_count": num_parallel_trials,
        "study_spec": {
            "metrics": [{
                "metric_id": "accuracy",
                "goal": StudySpec.MetricSpec.GoalType.MAXIMIZE,
            }],
            "parameters": [
                {"parameter_id": "batch_size",
                 "discrete_value_spec": {"values": [16, 32, 64]},
                 "scale_type": StudySpec.ParameterSpec.ScaleType.UNIT_LINEAR_SCALE},
                {"parameter_id": "learning_rate",
                 "double_value_spec": {"min_value": 1e-05, "max_value": 1e-02},
                 "scale_type": StudySpec.ParameterSpec.ScaleType.UNIT_LOG_SCALE},
                {"parameter_id": "num_layers",
                 "integer_value_spec": {"min_value": 3, "max_value": 50},
                 "scale_type": StudySpec.ParameterSpec.ScaleType.UNIT_LINEAR_SCALE},
                {"parameter_id": "num_neurons",
                 "integer_value_spec": {"min_value": 20, "max_value": 250},
                 "scale_type": StudySpec.ParameterSpec.ScaleType.UNIT_LINEAR_SCALE},
                {"parameter_id": "positive_class_weight",
                 "double_value_spec": {"min_value": 0.5, "max_value": 0.99},
                 "scale_type": StudySpec.ParameterSpec.ScaleType.UNIT_LINEAR_SCALE}
            ],
            "algorithm": StudySpec.Algorithm.ALGORITHM_UNSPECIFIED,  # Bayesian Optimization
        },
        "trial_job_spec": {"worker_pool_specs": [
            {"machine_spec": {
                "machine_type": "n1-standard-4",
                "accelerator_type": vertex_ai.gapic.AcceleratorType.ACCELERATOR_TYPE_UNSPECIFIED,
                "accelerator_count": 0,
            },
                "replica_count": 1,
                "python_package_spec": {
                    "executor_image_uri": training_image,
                    "package_uris": [training_package],
                    "python_module": tuner_module,
                    "args": ['--trial_epochs=10', '--trial_num_records=10000',
                             f'--dataset_path={dataset_artifact_path}'],
                }}
        ]},
    }

    response = client.create_hyperparameter_tuning_job(
        parent=parent,
        hyperparameter_tuning_job=hyperparameter_tuning_job_spec)

    return (response.name,)


@component(packages_to_install=["google-cloud-aiplatform"])
def parse_optimal_hyperparameters(resource_name: str, region: str) -> NamedTuple("Outputs", [("parameters", dict)]):
    from time import sleep
    from google.cloud import aiplatform as vertex_ai
    api_endpoint = f"{region}-aiplatform.googleapis.com"

    client = vertex_ai.gapic.JobServiceClient(client_options={"api_endpoint": api_endpoint})

    while True:
        response = client.get_hyperparameter_tuning_job(name=resource_name)
        if response.state.name == "JOB_STATE_RUNNING":
            sleep(60)
        else:
            trials = []

            for trial in response.trials:
                if trial.state.name == "SUCCEEDED":
                    data = dict()

                    for param in trial.parameters:
                        data[param.parameter_id] = param.value

                    data['id'] = int(trial.id)

                    for metric in trial.final_measurement.metrics:
                        data[metric.metric_id] = metric.value

                    trials.append(data)

            optimal_parameters = sorted(trials, key=lambda d: d['accuracy'], reverse=True)[0]

            return (optimal_parameters, )


@component(packages_to_install=["google-cloud-aiplatform"])
def create_tensorboard_instance(project_id: str, region: str, display_name: str) \
        -> NamedTuple("Outputs", [("tensorboard_instance", str)]):
    from google.cloud import aiplatform as vertex_ai

    instance = vertex_ai.Tensorboard.create(
        project=project_id,
        location=region,
        display_name=display_name,
    )

    return (instance.resource_name,)


@component(packages_to_install=["google-cloud-aiplatform"])
def train_model(project_id: str, region: str, display_name: str, dataset_instance: str, tensorboard_instance: str,
                python_package_gcs_uri: str, python_module_name: str, training_image: str, serving_image: str,
                staging_bucket: str, parameters: dict, train_epochs: int) \
        -> NamedTuple("Outputs", [("model_instance", str)]):
    from google.cloud import aiplatform as vertex_ai

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    dataset_resurce_name = resource_name(project_id, region, dataset_instance, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_resurce_name)

    training_job = vertex_ai.CustomPythonPackageTrainingJob(
        project=project_id,
        location=region,
        display_name=display_name,
        python_package_gcs_uri=python_package_gcs_uri,
        python_module_name=python_module_name,
        container_uri=training_image,
        model_serving_container_image_uri=serving_image,
        staging_bucket=staging_bucket
    )

    model = training_job.run(
        dataset=dataset,
        training_fraction_split=0.7,
        validation_fraction_split=0.2,
        test_fraction_split=0.1,
        tensorboard=tensorboard_instance,
        service_account="vertex-ai-tensorboard@coe-ie-dsg-training-lab.iam.gserviceaccount.com",
        replica_count=1,
        machine_type="n1-standard-4",
        accelerator_type="ACCELERATOR_TYPE_UNSPECIFIED",
        accelerator_count=0,
        args=[
            f'--batch_size={int(parameters["batch_size"])}',
            f'--learning_rate={parameters["learning_rate"]}',
            f'--num_layers={int(parameters["num_layers"])}',
            f'--num_neurons={int(parameters["num_neurons"])}',
            f'--epochs={train_epochs}',
            f'--positive_class_weight={parameters["positive_class_weight"]}'
        ]
    )

    return (model.resource_name,)


@component(base_image="us-docker.pkg.dev/vertex-ai/training/tf-cpu.2-6:latest", packages_to_install=['scikit-learn'])
def evaluate_classification_model(dataset_artifact_path: str, model_artifact_path: str,
                                  c_metrics: Output[ClassificationMetrics], metrics: Output[Metrics]):
    import logging
    import tensorflow as tf
    from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve, average_precision_score

    logging.getLogger().setLevel(logging.INFO)

    def is_test(x, y):
        return True if x['DataSplit'] == "TEST" else False

    AUTO = tf.data.experimental.AUTOTUNE
    dataset = tf.data.experimental.make_csv_dataset(
        file_pattern=dataset_artifact_path, header=True, label_name="Deposit",
        batch_size=1, num_parallel_reads=AUTO, prefetch_buffer_size=256, num_epochs=1)
    dataset = dataset.filter(is_test)

    model = tf.keras.models.load_model(model_artifact_path)

    y_proba = model.predict(dataset)
    y_pred = y_proba.argmax(axis=1)
    y_true = [e.numpy().item() for e in list(dataset.map(lambda x, y: y))]

    cm = confusion_matrix(y_true, y_pred)
    auc = roc_auc_score(y_true, y_proba.ravel().tolist())
    aucpr = average_precision_score(y_true, y_proba.ravel().tolist())

    fpr, tpr, thresholds = roc_curve(y_true, y_proba.ravel().tolist())

    logging.info(cm, auc, aucpr)

    metrics.log_metric("auRoc", auc)
    metrics.log_metric("auPrc", aucpr)

    c_metrics.log_roc_curve(fpr.tolist(), tpr.tolist(), thresholds.tolist())
    c_metrics.log_confusion_matrix(
        categories=["No Deposit", "Deposit"],
        matrix=cm.tolist())


@component(packages_to_install=["google-cloud-aiplatform"])
def export_model_artifact(model_resource_name: str, model_artifact: Output[Model]) \
        -> NamedTuple("Outputs", [("model_artifact_path", str)]):
    from google.cloud import aiplatform as vertex_ai

    model = vertex_ai.Model(model_resource_name)
    full_uri = model.export_model(
        export_format_id='custom-trained',
        artifact_destination=model_artifact.uri
    )
    artifact_path = full_uri['artifactOutputUri'].replace('gs://', '/gcs/')

    return (artifact_path,)


@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline_fn(project_id: str, region: str, dataset_instance: str, python_package_path: str, trainer_module_name: str,
                tuner_module_name: str,  training_image: str, serving_image: str, train_epochs: int,
                max_trial_count: int, num_parallel_trials: int):

    dataset_import_op = get_dataset_source_artifact(
        project_id=project_id, region=region, dataset_id=dataset_instance
    )

    data_validation_op = data_validation(
        dataset_artifact_path=dataset_import_op.outputs['dataset_artifact_path'], frozen_schema_path="None")

    data_validation_op.after(dataset_import_op)

    hyopt_job_op = create_hyperparameter_optimization_job(
        project_id=project_id, region=region, dataset_artifact_path=dataset_import_op.outputs['dataset_artifact_path'],
        tuner_module=tuner_module_name,
        training_image=training_image,
        training_package=python_package_path,
        max_trial_count=max_trial_count,
        num_parallel_trials=num_parallel_trials
    )

    hyopt_parse_op = parse_optimal_hyperparameters(
        resource_name=hyopt_job_op.outputs['job_resource_name'], region=region)

    tensorboard_create_op = create_tensorboard_instance(
        project_id=project_id,
        region=region,
        display_name="demo_tensorboard_instance"
    )

    tensorboard_create_op.after(hyopt_parse_op)

    train_op = train_model(
        parameters=hyopt_parse_op.outputs['parameters'],
        project_id=project_id,
        region=region,
        display_name="demo_tensorflow_model-v7-model",
        dataset_instance=dataset_instance,
        tensorboard_instance=tensorboard_create_op.outputs['tensorboard_instance'],
        python_package_gcs_uri=python_package_path,
        python_module_name=trainer_module_name,
        training_image=training_image,
        serving_image=serving_image,
        staging_bucket=f"{VERTEX_PIPELINES_ROOT}{PIPELINE_NAME}/staging_bucket",
        train_epochs=train_epochs
    )

    train_op.after(tensorboard_create_op)

    export_model_op = export_model_artifact(
        model_resource_name=train_op.outputs["model_instance"],
    )

    evaluation_op = evaluate_classification_model(
        dataset_artifact_path=dataset_import_op.outputs["dataset_artifact_path"],
        model_artifact_path=export_model_op.outputs["model_artifact_path"]
    )


if __name__ == "__main__":
    from kfp.v2 import compiler

    compiler.Compiler().compile(
        pipeline_func=pipeline_fn,
        package_path=f'{PIPELINE_NAME}.json')

    from google.cloud import aiplatform as vertex_ai
    from google.oauth2 import service_account
    from datetime import datetime

    TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
    PROJECT_ID = "coe-ie-dsg-training-lab"
    LOCATION = "europe-west2"

    job = vertex_ai.PipelineJob(
        project=PROJECT_ID,
        location=LOCATION,
        display_name=f"{PIPELINE_NAME}-{TIMESTAMP}",
        template_path=f'{PIPELINE_NAME}.json',
        enable_caching=True,
        parameter_values={
            "project_id": PROJECT_ID,
            "region": LOCATION,
            "dataset_instance": "2010435021155860480",
            "python_package_path": "gs://coe-ie-dsg-training-lab-demo/source_distributions/tensorflow-custom-training-demo-hyperparams-2.0.tar.gz",
            "trainer_module_name": "trainer.task",
            "tuner_module_name": "trainer.tuner",
            "training_image": "us-docker.pkg.dev/vertex-ai/training/tf-cpu.2-6:latest",
            "serving_image": "us-docker.pkg.dev/vertex-ai/prediction/tf2-cpu.2-6:latest",
            "train_epochs": 150,
            "max_trial_count": 10,
            "num_parallel_trials": 3
        }
    )

    job.run()
