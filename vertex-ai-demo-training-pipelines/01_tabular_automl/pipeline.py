from typing import NamedTuple
from kfp.v2.dsl import component, pipeline

VERTEX_PIPELINES_ROOT = 'gs://vertex-pipeline-metadata-store97/'
PIPELINE_NAME = 'demo-tabular-automl'


@component(packages_to_install=["google-cloud-aiplatform"])
def create_tabular_dataset(project_id: str, region: str, display_name: str, gcs_source: str) \
        -> NamedTuple("Outputs", [("dataset_instance", str)]):
    from google.cloud import aiplatform as vertex_ai

    instance = vertex_ai.TabularDataset.create(
        project=project_id,
        location=region,
        display_name=display_name,
        gcs_source=gcs_source,
    )

    return (instance.resource_name,)


@component(packages_to_install=["google-cloud-aiplatform"])
def train_automl_model(project_id: str, region: str, display_name: str, dataset_instance: str) \
        -> NamedTuple("Outputs", [("model_instance", str)]):

    from google.cloud import aiplatform as vertex_ai

    dataset = vertex_ai.TabularDataset(dataset_name=dataset_instance)

    training_job = vertex_ai.AutoMLTabularTrainingJob(
        project=project_id,
        location=region,
        display_name=f"{display_name}-training-pipeline",
        optimization_prediction_type="classification",
        optimization_objective="maximize-au-roc",
        column_specs={
            "Age": "numeric",
            "Job": "categorical",
            "MaritalStatus": "categorical",
            "Education": "categorical",
            "Default": "categorical",
            "Balance": "numeric",
            "Housing": "categorical",
            "Loan": "categorical",
            "Contact": "categorical",
            "Day": "categorical",
            "Month": "categorical",
            "Duration": "numeric",
            "Campaign": "categorical",
            "PDays": "numeric",
            "POutcome": "categorical",
            "DataSplit": "categorical"
        }
    )

    model = training_job.run(
        dataset=dataset,
        target_column="Deposit",
        predefined_split_column_name="DataSplit",
        budget_milli_node_hours=1000,
        model_display_name=f"{display_name}-model"
    )

    return (model.resource_name, )


@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline_fn(project_id: str, region: str, dataset_gcs_source: str):

    dataset_create_op = create_tabular_dataset(
        project_id=project_id,
        region=region,
        display_name="demo_tabular_dataset",
        gcs_source=dataset_gcs_source
    )

    train_op = train_automl_model(
        project_id=project_id,
        region=region,
        display_name="demo-tabular-automl",
        dataset_instance=dataset_create_op.outputs['dataset_instance'],
    )


if __name__ == "__main__":
    from kfp.v2 import compiler

    compiler.Compiler().compile(
        pipeline_func=pipeline_fn,
        package_path=f'{PIPELINE_NAME}.json')
    
    from google.cloud import aiplatform as vertex_ai
    from google.oauth2 import service_account
    from datetime import datetime

    TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
    PROJECT_ID = "nonprod-pcex-softserv97-lab"
    LOCATION = "us-central1"
    SERVICE_ACCOUNT = "vertex-notebook-sa@nonprod-pcex-softserv97-lab.iam.gserviceaccount.com"


    job = vertex_ai.PipelineJob(
        project=PROJECT_ID,
        location=LOCATION,
        credentials=credentials,
        display_name=f"{PIPELINE_NAME}-{TIMESTAMP}",
        template_path=f'{PIPELINE_NAME}.json',
        enable_caching=True,
        parameter_values={
            "project_id": PROJECT_ID,
            "region": LOCATION,
            "dataset_gcs_source": "gs://vertex-pipeline-binary-store97/data_tabular_tabular_split_bank_marketing_v2.csv",
        }
    )

    job.submit(service_account=SERVICE_ACCOUNT)
