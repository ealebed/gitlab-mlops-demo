from typing import NamedTuple
from kfp.v2.dsl import component, pipeline, Output, Metrics, ClassificationMetrics, Model

VERTEX_PIPELINES_ROOT = 'gs://coe-ie-dsg-training-lab-vertex-pipelines-root-bucket/'
PIPELINE_NAME = 'demo-tabular-dataset-tensorflow'
VERTEX_BUCKET = 'coe-ie-dsg-training-lab-demo'
VERTEX_SOURCE_DIR = 'source_distributions'


@component(packages_to_install=["google-cloud-aiplatform"])
def create_tensorboard_instance(project_id: str, region: str, display_name: str) \
        -> NamedTuple("Outputs", [("tensorboard_instance", str)]):
    from google.cloud import aiplatform as vertex_ai

    instance = vertex_ai.Tensorboard.create(
        project=project_id,
        location=region,
        display_name=display_name,
    )

    return (instance.resource_name,)


@component(packages_to_install=["google-cloud-aiplatform", "google-cloud-storage"])
def train_model(project_id: str, region: str, display_name: str, dataset_instance: str, tensorboard_instance: str,
                gs_bucket: str, source_dir: str, python_module_name: str, training_image: str, serving_image: str,
                staging_bucket: str) \
        -> NamedTuple("Outputs", [("model_instance", str)]):
    from google.cloud import aiplatform as vertex_ai
    from google.cloud import storage

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    dataset_resurce_name = resource_name(project_id, region, dataset_instance, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_resurce_name)

    # copy training module from GS
    client = storage.Client()
    bucket = client.get_bucket(gs_bucket)
    blob = storage.Blob(f'{source_dir}/{python_module_name}', bucket)
    with open(python_module_name, 'wb') as file_obj:
        client.download_blob_to_file(blob, file_obj)

    training_job = vertex_ai.CustomTrainingJob(
        project=project_id,
        location=region,
        display_name=display_name,
        script_path=python_module_name,
        container_uri=training_image,
        model_serving_container_image_uri=serving_image,
        staging_bucket=staging_bucket
    )

    model = training_job.run(
        dataset=dataset,
        training_fraction_split=0.7,
        validation_fraction_split=0.2,
        test_fraction_split=0.1,
        tensorboard=tensorboard_instance,
        service_account="vertex-ai-tensorboard@coe-ie-dsg-training-lab.iam.gserviceaccount.com",
        replica_count=1,
        machine_type="n1-standard-4",
        accelerator_type="ACCELERATOR_TYPE_UNSPECIFIED",
        accelerator_count=0
    )

    return (model.resource_name,)


@component(base_image="us-docker.pkg.dev/vertex-ai/training/tf-cpu.2-6:latest", packages_to_install=['scikit-learn'])
def evaluate_classification_model(dataset_artifact_path: str, model_artifact_path: str,
                                  c_metrics: Output[ClassificationMetrics], metrics: Output[Metrics]):
    import logging
    import tensorflow as tf
    from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve, average_precision_score

    logging.getLogger().setLevel(logging.INFO)

    def is_test(x, y):
        return True if x['DataSplit'] == "TEST" else False

    AUTO = tf.data.experimental.AUTOTUNE
    dataset = tf.data.experimental.make_csv_dataset(
        file_pattern=dataset_artifact_path, header=True, label_name="Deposit",
        batch_size=1, num_parallel_reads=AUTO, prefetch_buffer_size=256, num_epochs=1)
    dataset = dataset.filter(is_test)

    model = tf.keras.models.load_model(model_artifact_path)

    y_proba = model.predict(dataset)
    y_pred = y_proba.argmax(axis=1)
    y_true = [e.numpy().item() for e in list(dataset.map(lambda x, y: y))]

    cm = confusion_matrix(y_true, y_pred)
    auc = roc_auc_score(y_true, y_proba.ravel().tolist())
    aucpr = average_precision_score(y_true, y_proba.ravel().tolist())

    fpr, tpr, thresholds = roc_curve(y_true, y_proba.ravel().tolist())

    logging.info(cm, auc, aucpr)

    metrics.log_metric("auRoc", auc)
    metrics.log_metric("auPrc", aucpr)

    c_metrics.log_roc_curve(fpr.tolist(), tpr.tolist(), thresholds.tolist())
    c_metrics.log_confusion_matrix(
        categories=["No Deposit", "Deposit"],
        matrix=cm.tolist())


@component(packages_to_install=["google-cloud-aiplatform"])
def export_model_artifact(model_resource_name: str, model_artifact: Output[Model]) \
        -> NamedTuple("Outputs", [("model_artifact_path", str)]):
    from google.cloud import aiplatform as vertex_ai

    model = vertex_ai.Model(model_resource_name)
    full_uri = model.export_model(
        export_format_id='custom-trained',
        artifact_destination=model_artifact.uri
    )
    artifact_path = full_uri['artifactOutputUri'].replace('gs://', '/gcs/')

    return (artifact_path,)


@component(packages_to_install=["google-cloud-aiplatform"])
def get_dataset_source_artifact(project_id: str, region: str, dataset_id: str) \
        -> NamedTuple("Outputs", [("dataset_artifact_path", str)]):
    from google.cloud import aiplatform as vertex_ai

    def resource_name(project_id, region, resource_id, resource_type):
        return f"projects/{project_id}/locations/{region}/{resource_type}/{resource_id}"

    dataset_resource_name = resource_name(project_id, region, dataset_id, "datasets")
    dataset = vertex_ai.TabularDataset(dataset_resource_name)

    dataset_uri = dataset.to_dict()['metadata']['inputConfig']['gcsSource']['uri'][0]
    dataset_path = dataset_uri.replace('gs://', '/gcs/')

    return (dataset_path,)


@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline_fn(project_id: str, region: str, dataset_instance: str,
                gs_bucket: str, source_dir: str, python_module_name: str,
                training_image: str, serving_image: str):
    tensorboard_create_op = create_tensorboard_instance(
        project_id=project_id,
        region=region,
        display_name="demo_tensorboard_instance"
    )

    train_op = train_model(
        project_id=project_id,
        region=region,
        display_name="demo_tensorflow_model-model",
        dataset_instance=dataset_instance,
        tensorboard_instance=tensorboard_create_op.outputs['tensorboard_instance'],
        gs_bucket=gs_bucket,
        source_dir=source_dir,
        python_module_name=python_module_name,
        training_image=training_image,
        serving_image=serving_image,
        staging_bucket=f"{VERTEX_PIPELINES_ROOT}{PIPELINE_NAME}/staging_bucket"
    )

    export_model_op = export_model_artifact(
        model_resource_name=train_op.outputs["model_instance"],
    )

    dataset_import_op = get_dataset_source_artifact(
        project_id=project_id, region=region, dataset_id=dataset_instance
    )

    evaluation_op = evaluate_classification_model(
        dataset_artifact_path=dataset_import_op.outputs["dataset_artifact_path"],
        model_artifact_path=export_model_op.outputs["model_artifact_path"]
    )


if __name__ == "__main__":
    from kfp.v2 import compiler
    import logging

    logging.getLogger().setLevel(logging.INFO)

    compiler.Compiler().compile(
        pipeline_func=pipeline_fn,
        package_path=f'{PIPELINE_NAME}.json')

    from google.cloud import aiplatform as vertex_ai
    from google.oauth2 import service_account
    from datetime import datetime

    TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
    PROJECT_ID = "coe-ie-dsg-training-lab"
    LOCATION = "europe-west2"

    # copy task.py training module to GS bucket
    from google.cloud import storage
    import os

    client = storage.Client()
    save_directory = f'gs://{VERTEX_BUCKET}/{VERTEX_SOURCE_DIR}'
    training_module = 'task.py'
    storage_path = os.path.join(save_directory, training_module)
    blob = storage.blob.Blob.from_string(storage_path, client=client)
    blob.upload_from_filename(training_module)

    job = vertex_ai.PipelineJob(
        project=PROJECT_ID,
        location=LOCATION,
        display_name=f"{PIPELINE_NAME}-{TIMESTAMP}",
        template_path=f'{PIPELINE_NAME}.json',
        enable_caching=True,
        parameter_values={
            "project_id": PROJECT_ID,
            "region": LOCATION,
            "dataset_instance": "2010435021155860480",
            "gs_bucket": VERTEX_BUCKET,
            "source_dir": VERTEX_SOURCE_DIR,
            "python_module_name": training_module,
            "training_image": "us-docker.pkg.dev/vertex-ai/training/tf-cpu.2-6:latest",
            "serving_image": "us-docker.pkg.dev/vertex-ai/prediction/tf2-cpu.2-6:latest"
        }
    )

    job.run()
