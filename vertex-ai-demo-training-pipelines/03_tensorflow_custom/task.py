import os
import pprint
import logging

import tensorflow as tf
from tensorflow import feature_column as fc
from tensorflow.keras import layers

# without this no logs will be visible in Vertex AI
logging.getLogger().setLevel(logging.INFO)

variables = [
    'AIP_DATA_FORMAT',  # The format that your dataset is exported in. Possible values include: jsonl, csv, or bigquery.
    'AIP_TRAINING_DATA_URI',  # The location that your training data is stored at.
    'AIP_VALIDATION_DATA_URI',  # The location that your validation data is stored at.
    'AIP_TEST_DATA_URI',  # The location that your test data is stored at.
    'AIP_MODEL_DIR',  # a Cloud Storage URI of a directory intended for saving model artifacts
    'AIP_CHECKPOINT_DIR',  # a Cloud Storage URI of a directory intended for saving checkpoints
    'AIP_TENSORBOARD_LOG_DIR'  # a Cloud Storage URI of a directory intended for saving TensorBoard logs
]

for variable in variables:
    logging.info(f'os.environ[{variable}] = {os.environ[variable]}')
    if variable not in os.environ:
        raise KeyError(f'The {variable} environment variable has not been set')

logging.info("loading the datasets")
AUTO = tf.data.experimental.AUTOTUNE
dataset = {}

DUMMY_COLUMN = "DataSplit"
TARGET_COLUMN = "Deposit"
NUMERIC_COLUMNS = ['Age', 'Balance', 'Duration', 'PDays']
CATEGORICAL_COLUMNS = ['Job', 'MaritalStatus', 'Education', 'Default', 'Housing', 'Loan', 'Contact', 'Month', 'POutcome']
ORDINAL_COLUMNS = ['Day', 'Campaign', 'Previous']

for ds, source in zip(['train', 'valid', 'test'], ['AIP_TRAINING_DATA_URI', 'AIP_VALIDATION_DATA_URI', 'AIP_TEST_DATA_URI']):
    dataset[ds] = tf.data.experimental.make_csv_dataset(
        file_pattern=os.environ[source], header=True, label_name=TARGET_COLUMN,
        batch_size=32, num_parallel_reads=AUTO, prefetch_buffer_size=256, num_epochs=1)
    dataset[ds].cache()


def get_normalization_layer(name, dataset):
    normalizer = layers.experimental.preprocessing.Normalization(axis=None)
    feature_dataset = dataset.map(lambda x, y: x[name])
    normalizer.adapt(feature_dataset)
    return normalizer


def get_one_hot_encoding_layer(name, dataset, dtype, max_tokens=None):
    if dtype=='string':
        index = layers.experimental.preprocessing.StringLookup(max_tokens=max_tokens)
    else:
        index = layers.experimental.preprocessing.IntegerLookup(max_tokens=max_tokens)

    feature_dataset = dataset.map(lambda x, y: x[name])
    index.adapt(feature_dataset)
    encoder = layers.experimental.preprocessing.CategoryEncoding(num_tokens=index.vocabulary_size(),
                                                                 output_mode="one_hot")
    return lambda feature: encoder(index(feature))


inputs = []
dense_features = []
sparse_features = []

for column in NUMERIC_COLUMNS:
    input_layer = layers.Input(shape=(1,), name=column, dtype='float32')
    normalization_layer = get_normalization_layer(column, dataset['train'])
    numeric_feature = normalization_layer(input_layer)

    inputs.append(input_layer)
    dense_features.append(numeric_feature)

for column in CATEGORICAL_COLUMNS:
    dtype='string'
    input_layer = layers.Input(shape=(1,), name=column, dtype=dtype)
    encoding_layer = get_one_hot_encoding_layer(column, dataset['train'], dtype, max_tokens=30)
    numeric_feature = encoding_layer(input_layer)

    inputs.append(input_layer)
    sparse_features.append(numeric_feature)

for column in ORDINAL_COLUMNS:
    dtype='int32'
    input_layer = layers.Input(shape=(1,), name=column, dtype=dtype)
    encoding_layer = get_one_hot_encoding_layer(column, dataset['train'], dtype, max_tokens=30)
    numeric_feature = encoding_layer(input_layer)

    inputs.append(input_layer)
    sparse_features.append(numeric_feature)

logging.info('Creating the Keras Model')


def wide_and_deep(sparse_features, dense_features, num_layers=3, num_neurons=5):
    wide = layers.concatenate(sparse_features)
    deep = layers.concatenate(dense_features)

    for layer in range(num_layers):
        deep = layers.Dense(num_neurons, activation='relu')(deep)

    combined = layers.concatenate([wide, deep])
    output = layers.Dense(1, activation='sigmoid')(combined)

    model = tf.keras.Model(inputs, output)
    model.compile(
        optimizer='adam',
        loss='binary_crossentropy',
        metrics='accuracy')

    model.summary()
    return model


model = wide_and_deep(sparse_features, dense_features)

logging.info('Defining Callbacks for Training')
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=os.environ['AIP_CHECKPOINT_DIR'],
    save_weights_only=False,
    monitor='accuracy',
    mode='max',
    save_best_only=True)

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=os.environ['AIP_TENSORBOARD_LOG_DIR'])

logging.info('Start model training')
model.fit(dataset['train'], epochs=10, validation_data=dataset['valid'],
          callbacks=[model_checkpoint_callback, tensorboard_callback],
          class_weight={0: 0.1, 1: 0.9})

logging.info('Loading the best performing model')
best_model = tf.keras.models.load_model(os.environ['AIP_CHECKPOINT_DIR'])

logging.info('Evaluating the best model on the test dataset')
loss, accuracy = best_model.evaluate(dataset['test'])
logging.info(f'Test loss = {loss} / test accuracy = {accuracy}')
print(best_model.predict(dataset['test']))

logging.info('Saving the model to GCS')
best_model.save(os.environ['AIP_MODEL_DIR'])

logging.info("ALL DONE!")
