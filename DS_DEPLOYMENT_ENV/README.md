# GitLab repo template deployment environment

## Prerequisites

### 1. Create a Service Account
- In the Google Cloud Console project you want to push the final image to, you should create Service Account with **Storage Admin** and **Artifact Registry Writer** permissions;
- Download a JSON key for this service account
**This SA should be created automatically in LZ training project at terraform resources provisioning step**

### 2. OPTIONAL: Base64 encode downloaded GCP Service Account key:
MacOS:
```bash
base64 ~/key.json > ~/key-base64.json
```

Linux:
```bash
base64 -w0 ~/key.json > ~/key-base64.json
```
The `-w0` option ensures the encoded output is formatted as a single line.

### 3. Create an environment variable in your GitLab project
Navigate to **Settings > CI/CD** in your project (or Group) and expand the **Variables** section. Click on **Add Variable**.

(!) If your branch is NOT protected, then your variable in GitLab should also be NOT protected. [More info](https://github.com/JanMikes/gitlab-ci-push-to-gcr/issues/2#issue-552452892)

Name your **Key** (e.g. *GCP_SA_KEY*) and paste the contents of your base64 encoded Service Account key from the previous step into the **Value** field. Be sure to select ‘**File**’ as the variable **Type**. Check the **Mask variable** option (and the **Protect variable** option if you require it). Click on **Add variable** to finish adding the variable to your project.

## Description GitLab CI/CD pipeline

Deployment GitLab CI/CD pipeline for Data Science team also has only one step:
 
![Steps](./images/deployment-step.png)

The `run-deployment-pipeline` step is aimed to run Vertex AI Pipeline in deployment environment and create endpoints to serve online predictions.
