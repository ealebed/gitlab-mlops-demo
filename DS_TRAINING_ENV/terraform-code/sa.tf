resource "google_service_account" "custom_service_account" {
  project      = local.local_data.gcp_project
  account_id   = "custom-sa"
  display_name = "Custom SA"
  description  = "Custom Service Account for testing purpose"
}

resource "google_project_iam_member" "project" {
  for_each = toset([ 
    "roles/storage.admin",
    "roles/compute.admin"
  ])
  
  project = local.local_data.gcp_project
  role    = each.key
  member  = "serviceAccount:${google_service_account.custom_service_account.email}"
}
