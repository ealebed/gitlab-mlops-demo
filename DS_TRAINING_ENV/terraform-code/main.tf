# terraform configuration
terraform {
  required_version = ">= 0.13"
  required_providers {

    google = {
      source  = "hashicorp/google"
      version = ">= 3.53, < 5.0"
    }
  }

  backend "remote" {
    hostname = "app.terraform.io"
    organization = "ealebed"

    workspaces {
      name = "gitlab-tfc-demo"
    }
  }

}

# Common for all labs
locals {
  local_data = jsondecode(var.varSet)
}

# Configure Google Provider
provider "google" {
  project = local.local_data.gcp_project
  region  = local.local_data.gcp_region
  zone    = local.local_data.gcp_zone
}

# Variables
variable "varSet" {}
