resource "google_compute_instance" "demo" {
  name = "demo-1"
  machine_type = local.local_data.machine_type

  boot_disk {
    initialize_params {
      image = local.local_data.image
    }
  }

  network_interface {
    network = "default"

    access_config {
        // Ephemeral IP
    }
  }
}
