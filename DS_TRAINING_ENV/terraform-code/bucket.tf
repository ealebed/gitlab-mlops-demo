resource "google_storage_bucket" "store" {
  name          = "${local.local_data.gcp_project}-ml-store"
  location      = local.local_data.location
  project       = local.local_data.gcp_project
  storage_class = local.local_data.storage_class
  force_destroy = true

  versioning {
    enabled = "false"
  }
}
