# GitLab repo template training environment

## Prerequisites

### 1. Create a Service Account
- In the Google Cloud Console project you want to push the final image to, you should create Service Account with **Storage Admin** and **Artifact Registry Writer** permissions;
- Download a JSON key for this service account
**This SA should be created automatically in LZ training project at terraform resources provisioning step**

### 2. OPTIONAL: Base64 encode downloaded GCP Service Account key:
MacOS:
```bash
base64 ~/key.json > ~/key-base64.json
```

Linux:
```bash
base64 -w0 ~/key.json > ~/key-base64.json
```
The `-w0` option ensures the encoded output is formatted as a single line.

### 3. Create an environment variable in your GitLab project
Navigate to **Settings > CI/CD** in your project (or Group) and expand the **Variables** section. Click on **Add Variable**.

(!) If your branch is NOT protected, then your variable in GitLab should also be NOT protected. [More info](https://github.com/JanMikes/gitlab-ci-push-to-gcr/issues/2#issue-552452892)

Name your **Key** (e.g. *GCP_SA_KEY*) and paste the contents of your base64 encoded Service Account key from the previous step into the **Value** field. Be sure to select ‘**File**’ as the variable **Type**. Check the **Mask variable** option (and the **Protect variable** option if you require it). Click on **Add variable** to finish adding the variable to your project.

## Description GitLab CI/CD pipeline

The GitLab CI/CD pipeline consists from stages and steps described below:

![Stages](./images/stages.png)

All steps, associated with terraform commands, belong to the same stage `apply-terraform-changes` and have the following job dependencies:

![Stages](./images/tf-steps.png)

The other steps mainly related to MLOps are divided into several stages, with the following job dependencies:

![Stages](./images/ml-steps.png)

By default the GitLab CI/CD configuration file is `.gitlab-ci.yml` in the repository root folder. This file is self documented and well commented.

The `variables` section store information, that can be used in all job scripts in the pipeline. For security reasons, sensitive information (like passwords and secret keys) should be added on **Settings > CI/CD** in your project (or Group) and masked (hidden in job logs). Adding sensitive information to custom GitLab runner also can be an option.

The `default` section contains custom default values for job keywords (e. g. `tags` for selecting a specific runner from the list of all runners that are available for the project).

*OPTIONAL:* Inside the pipeline can be used YAML anchors - a feature which let identify an item and then reference it elsewhere in file. Anchors are created using the `&` sign. The sign is followed by an alias name. Usage aliases and anchors is aimed at reducing repeated blocks/similar configurations ("Don't repeat yourself" principle). Anchor declaration example:

```yaml
.kaniko-before-script: &kaniko-before-script
  - export GOOGLE_APPLICATION_CREDENTIALS=/kaniko/kaniko-secret.json
  - base64 -d ${GCP_SA_KEY} > ${GOOGLE_APPLICATION_CREDENTIALS}
```

Anchor usage example:

```yaml
build-training-image:
  stage: build-images
  script:
    - *kaniko-before-script
...
```

There are several stages in this CI/CD pipeline: `apply-terraform-changes`, `initialize`, `build-images`, `pack-pipeline`, `run-pipeline`. Some of them contains more than one step inside.

All steps from `apply-terraform-changes` stage should run only on terraform code changes:

- the `terraform-init` step used to prepare working directory and remote terraform backend for execution other commands and check whether the terraform configuration is valid;

- the `terraform-plan` step needed to show changes required by the current terraform configuration;

- the `terraform-apply` step used to create (or update) infrastructure to achieve state described in terraform code. This step should be executed manually, from GitLab UI;

- the `terraform-destroy` step used to destroy previously created terraform infrastructure. This step should be executed manually, from GitLab UI - use this wisely.

OPTIONAL: Between `plan` and `apply` stages, HashiCorp Sentinel Policy check can be executed on Terraform Enterprise's side. To achieve this, Terraform Enterprise workspace should be attached to Sentinel Policy Set.

OPTIONAL: Custom step for terraform code validation (e.g. `wiz-scan` for WIZ.io policy checks) can be added to this stage.

The other steps should always be performed, regardless of whether the terraform code has changed or not.

- the `init` step used to create environment variable "VERSION" (based on current date) that can be used in all job scripts in the pipeline.

- the `build-training-image` and the `build-serving-image` steps are needed to build and push docker images to Google Artifact Registry.

- the `pack-pipeline` step used to compile versioned JSON Vertex AI Pipeline from 'python.py' with substitutions and upload it into Google Cloud Storage.

- the `run-training-pipeline` step used to run JSON Vertex AI Pipeline in training environment.
