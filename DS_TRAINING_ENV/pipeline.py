import matplotlib.pyplot as plt
import pandas as pd

from kfp.v2 import compiler, dsl
from kfp.v2.dsl import pipeline, component, Artifact, Dataset, Input, Metrics, Model, Output, InputPath, OutputPath

from google.cloud import aiplatform

# We'll use this namespace for metadata querying
from google.cloud import aiplatform_v1

REGION="${LOCATION}"
PROJECT_ID="${PROJECT_ID}"
BASE_IMAGE = "python:3.9"
TRAINING_IMAGE = "${T_IMAGE}:${VERSION}"
SERVING_IMAGE = "${S_IMAGE}:${VERSION}"
VERSION = "${VERSION}"
VERTEX_PIPELINES_ROOT = "gs://${BUCKET_NAME}/pipeline_root/"
PIPELINE_NAME = "mlmd-pipeline-${VERSION}"

@component(packages_to_install=["google-cloud-bigquery", "pandas", "pyarrow"], base_image=BASE_IMAGE, output_component_file="create_dataset.yaml")
def get_dataframe(bq_table: str, output_data_path: OutputPath("Dataset")):
    from google.cloud import bigquery
    import pandas as pd

    bqclient = bigquery.Client()
    table = bigquery.TableReference.from_string(bq_table)
    rows = bqclient.list_rows(table)
    dataframe = rows.to_dataframe(create_bqstorage_client=True)
    dataframe = dataframe.sample(frac=1, random_state=2)
    dataframe.to_csv(output_data_path)
    
@component(packages_to_install=["sklearn", "pandas", "joblib"], base_image=TRAINING_IMAGE, output_component_file="beans_model_component.yaml")
def sklearn_train(dataset: Input[Dataset], metrics: Output[Metrics], model: Output[Model]):
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.metrics import roc_curve
    from sklearn.model_selection import train_test_split
    from joblib import dump

    import pandas as pd
    df = pd.read_csv(dataset.path)
    labels = df.pop("Class").tolist()
    data = df.values.tolist()
    x_train, x_test, y_train, y_test = train_test_split(data, labels)

    skmodel = DecisionTreeClassifier()
    skmodel.fit(x_train,y_train)
    score = skmodel.score(x_test,y_test)
    print('accuracy is:',score)

    metrics.log_metric("accuracy",(score * 100.0))
    metrics.log_metric("framework", "Scikit Learn")
    metrics.log_metric("dataset_size", len(df))
    dump(skmodel, model.path + ".joblib")
    
@component(packages_to_install=["google-cloud-aiplatform"], base_image=SERVING_IMAGE, output_component_file="beans_deploy_component.yaml")
def deploy_model(model: Input[Model], project: str, region: str, vertex_endpoint: Output[Artifact], vertex_model: Output[Model]):
    from google.cloud import aiplatform

    aiplatform.init(project=project, location=region)

    deployed_model = aiplatform.Model.upload(
        display_name="beans-model-pipeline",
        artifact_uri = model.uri.replace("model", ""),
        serving_container_image_uri="us-docker.pkg.dev/vertex-ai/prediction/sklearn-cpu.0-24:latest"
    )
    endpoint = deployed_model.deploy(machine_type="n1-standard-4")

    # Save data to the output params
    vertex_endpoint.uri = endpoint.resource_name
    vertex_model.uri = deployed_model.resource_name

@pipeline(pipeline_root=VERTEX_PIPELINES_ROOT, name=PIPELINE_NAME)
def pipeline(bq_table: str = "", output_data_path: str = "data.csv", project: str = PROJECT_ID, region: str = REGION):
    
    dataset_task = get_dataframe(bq_table)

    model_task = sklearn_train(dataset_task.output)

    deploy_task = deploy_model(
        model=model_task.outputs["model"],
        project=project,
        region=region
    )

compiler.Compiler().compile(pipeline_func=pipeline, package_path=f'{PIPELINE_NAME}.json')
