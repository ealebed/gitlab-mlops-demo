stages:
- initialize
- build-images
- pack-pipeline
- run-pipeline

variables:
  LOCATION: us-central1 # Regional location of the repository where the image is stored AND where the pipeline should be runned
  SA_KEY: ${TRN_SA_KEY}
  PROJECT_ID: ${TRN_PROJECT_ID}
  ARTIFACTORY_HOSTNAME: "${LOCATION}-docker.pkg.dev" # Google Artifact registry hostname
  JFROG_HOSTNAME: "acme-dockerv2-virtual.jfrog.io" # JUST FOR EXAMPLE
  JFROG_USER: "docker-deployer" # JUST FOR EXAMPLE
  JFROG_API_KEY: "" # Here should be a token for JFrog artifactory
  REPOSITORY_NAME: "vertex-docker-repo" # Name of the repository where the image is stored
  T_IMAGE: "${REPOSITORY_NAME}/training" # Training docker image's name
  S_IMAGE: "${REPOSITORY_NAME}/serving" # Serving docker image's name

default:
  tags:
  - gcp-runner

# Authorize in JFrog
.docker-login-script: &docker-login-script
# Login to Google Container Registry
- echo ${SA_KEY} > tmpfile.txt
- base64 -d tmpfile.txt | docker login "https://${ARTIFACTORY_HOSTNAME}" --username _json_key --password-stdin
# Login to JFrog container Registry
- echo ${JFROG_API_KEY} | docker login ${JFROG_HOSTNAME} --username ${JFROG_USER} --password-stdin

# Get service account key from secter ENV variables and authorize in GCP
.gcloud-before-script: &gcloud-before-script
  - echo ${SA_KEY} > tmpfile.txt
  - base64 -d tmpfile.txt > google_application_credentials_file.json
  - gcloud auth activate-service-account --key-file google_application_credentials_file.json
  - export GOOGLE_APPLICATION_CREDENTIALS=google_application_credentials_file.json
  - gcloud config set project ${PROJECT_ID}

# generate readable/understandable VERSION from date/time instead using git SHA
initialize:
  stage: initialize
  script:
  - echo "VERSION=$(date -u '+%y%m%d-%H%M')" >> build.env
  artifacts:
    reports:
      dotenv: build.env

# build training docker image and push to google artifact registry
build-training-image:
  stage: build-images
  script:
  - cd ${TRN_DIR}
  # - *docker-login-script
  - echo "build training docker image and push to JFrog for security analysis"
  # - >-
  #   docker build 
  #   --file Dockerfile.training 
  #   --tag "${JFROG_HOSTNAME}/${T_IMAGE}:${VERSION}"
  #   --tag "${ARTIFACTORY_HOSTNAME}/${PROJECT_ID}/${T_IMAGE}:${VERSION}" .
  # - docker push "${JFROG_HOSTNAME}/${T_IMAGE}:${VERSION}"
  # - echo "push training docker image to google artifact registry"
  # - docker push "${ARTIFACTORY_HOSTNAME}/${PROJECT_ID}/${T_IMAGE}:${VERSION}"
  needs: [ "initialize" ]
  dependencies:
  - initialize

# build serving docker image and push to google artifact registry
build-serving-image:
  stage: build-images
  script:
  - cd ${TRN_DIR}
  # - *docker-login-script
  - echo "build serving docker image and push to JFrog for security analysis"
  # - >-
  #   docker build 
  #   --file Dockerfile.serving 
  #   --tag "${JFROG_HOSTNAME}/${S_IMAGE}:${VERSION}"
  #   --tag "${ARTIFACTORY_HOSTNAME}/${PROJECT_ID}/${S_IMAGE}:${VERSION}" .
  # - docker push "${JFROG_HOSTNAME}/${S_IMAGE}:${VERSION}"
  # - echo "push serving docker image to google artifact registry"
  # - docker push "${ARTIFACTORY_HOSTNAME}/${PROJECT_ID}/${S_IMAGE}:${VERSION}"
  needs: [ "initialize" ]
  dependencies:
  - initialize

# compile versioned JSON pipeline from 'python.py' with substitutions and upload it into google bucket
# (!) This step should be checked with MLOps in customer ENV
pack-pipeline:
  stage: pack-pipeline
  script:
  - cd ${TRN_DIR}
  # - *gcloud-before-script
  # - apt-get install gettext-base
  # - pip3 install kfp --upgrade --quiet && export PATH=${PATH}:~/.local/bin
  # - pip3 install --quiet --no-cache-dir -r requirements.txt
  # - >-
  #   envsubst '${VERSION} ${T_IMAGE} ${S_IMAGE} ${BUCKET_NAME} ${PROJECT_ID} ${LOCATION}' < pipeline.py > pipeline-${VERSION}.py
  - echo "PIPELINE_ID=mlmd-pipeline-${VERSION}" >> build.env
  # Compile pipeline and store it in bucket
#   - python3 pipeline-${VERSION}.py
#   - gsutil cp *pipeline*.json gs://${BUCKET_NAME}/pipelines-json/
  - gsutil cp pipeline.py gs://${BUCKET_NAME}/pipelines-json/
  needs: [ "initialize", "build-training-image", "build-serving-image" ]
  dependencies:
  - initialize
  # artifacts:
  #   reports:
  #     dotenv: build.env
  #   paths:
  #     - "*pipeline*.json"

# run pipeline (just compiled or previosly uploaded to bucket)
# (!) This step should be checked with MLOps in customer ENV
run-training-pipeline:
  stage: run-pipeline
  script:
  - cd ${TRN_DIR}
  # - *gcloud-before-script
  # - export PIPELINE_TO_RUN=${PIPELINE_ID}
  # - echo "Run pipeline ${PIPELINE_TO_RUN}"
  # - apt-get install gettext-base
  # - pip3 install google-cloud-aiplatform --quiet
  # - >-
  #   envsubst '${PROJECT_ID} ${LOCATION} ${PIPELINE_TO_RUN}' < run-pipeline.tmpl > run-pipeline.py
  # - python3 run-pipeline.py
  needs: [ "initialize", "pack-pipeline" ]
  dependencies:
  - initialize
  - pack-pipeline

# run pipeline (just compiled or previosly uploaded to bucket)
# (!) This step should be checked with MLOps in customer ENV
run-validation-pipeline:
  stage: run-pipeline
  script:
  - cd ${TRN_DIR}
  # - echo ${VAL_SA_KEY} > tmpfile.txt
  # - base64 -d tmpfile.txt > google_validation_credentials_file.json
  # - gcloud auth activate-service-account --key-file google_validation_credentials_file.json
  # - export GOOGLE_APPLICATION_CREDENTIALS=google_validation_credentials_file.json
  # - gcloud config set project ${VAL_PROJECT_ID}
  - echo "Run pipeline in validation project"
  # - > 
  #   if [ ! -z "${PIPELINE_ID}" ]; then
  #     echo "Upload pipeline ${PIPELINE_ID} previosly stored in GCS bucket"
  #     gsutil cp gs://${BUCKET_NAME}/pipelines-json/${PIPELINE_ID}.json .
  #     export PIPELINE_TO_RUN=${PIPELINE_ID}
  #     echo "Run pipeline ${PIPELINE_TO_RUN}"
  #     apt-get install gettext-base
  #     pip3 install google-cloud-aiplatform --quiet
  #     envsubst '${VAL_PROJECT_ID} ${LOCATION} ${PIPELINE_TO_RUN}' < run-pipeline.tmpl > run-pipeline.py
  #     python3 run-pipeline.py
  #   else
  #     echo "Please, provide value to variable 'PIPELINE_ID'"
  #   fi
  needs: [ "initialize", "pack-pipeline" ]
  dependencies:
  - initialize
  - pack-pipeline
  when: manual
